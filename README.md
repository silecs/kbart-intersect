kbart-intersect
===============

Compute the intersection of a Koha export (XLSX) with a KBART file, using ISBN to match entries.

Typical usage:
```{bash}
./kbart-intersect isbn-report.xlsx kbart.txt > result.csv

# batch apply
for f in *.txt; do ./kbart-intersect isbn-report.xlsx "$f" > "${f}_result.csv"; done
```
