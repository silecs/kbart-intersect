module gitlab.com/silecs/kbart-intersect

go 1.15

require (
	github.com/tealeg/xlsx/v3 v3.2.0
	modernc.org/sqlite v1.7.3
)
