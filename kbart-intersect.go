package main

import (
	"database/sql"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/tealeg/xlsx/v3"

	_ "modernc.org/sqlite"
)

type context struct {
	db          *sql.DB
	insertKbart *sql.Stmt
	insertKoha  *sql.Stmt
}

func main() {
	flag.Parse()
	files := flag.Args()
	if len(files) != 2 {
		fmt.Println("kbart-intersect koha-export.xlsx kbart.txt")
		os.Exit(2)
	}

	var err error

	db, err := sql.Open("sqlite", "/tmp/kbart-intersect.sqlite")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	context := context{}
	context.db = db
	context.initDB()

	// import the XLSX file
	wb, err := xlsx.OpenFile(files[0])
	if err != nil {
		panic(err)
	}
	sheet := wb.Sheets[0]
	err = sheet.ForEachRow(context.visitKohaRow)

	// import the KBART file
	kbartFile, err := os.Open(files[1])
	if err != nil {
		panic(err)
	}
	kbartReader := csv.NewReader(kbartFile)
	kbartReader.Comma = '\t'
	rowPos := 0
	for {
		rowPos = rowPos + 1
		record, err := kbartReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		err = context.visitKbartRow(rowPos, record)
		if err != nil {
			panic(err)
		}
	}

	context.intersect()
}

func (c *context) initDB() {
	c.db.Exec("PRAGMA synchronous = OFF")
	c.db.Exec("PRAGMA encoding = 'UTF-8'")
	c.db.Exec("DROP TABLE IF EXISTS koha")
	c.db.Exec("CREATE TABLE koha (row int, isbn text, cote text, biblionumber int, pret text, titre text, auteur text)")
	c.db.Exec("DROP TABLE IF EXISTS kbart")
	c.db.Exec("CREATE TABLE kbart (row int, isbn text, url text, titre text)")

	var err error

	c.insertKoha, err = c.db.Prepare("INSERT INTO koha VALUES (?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		panic(err)
	}

	c.insertKbart, err = c.db.Prepare("INSERT INTO kbart VALUES (?, ?, ?, ?)")
	if err != nil {
		panic(err)
	}
}

func (c context) visitKohaRow(r *xlsx.Row) error {
	rawIsbns := r.GetCell(1).Value
	if rawIsbns == "" || rawIsbns == "ISBN" {
		return nil
	}
	isbns := strings.Split(rawIsbns, " | ")
	for _, isbn := range isbns {
		isbnNoCaret := strings.ReplaceAll(isbn, "-", "")
		_, err := c.insertKoha.Exec(r.GetCoordinate(), isbnNoCaret, r.GetCell(0).Value, r.GetCell(5).Value, r.GetCell(2).Value, r.GetCell(3).Value, r.GetCell(4).Value)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c context) visitKbartRow(rowPos int, row []string) error {
	isbnPaper := row[1]
	if isbnPaper != "" && isbnPaper != "print_identifier" {
		isbnNoCaret := strings.ReplaceAll(isbnPaper, "-", "")
		_, err := c.insertKbart.Exec(rowPos, isbnNoCaret, row[9], row[0])
		if err != nil {
			return err
		}
	}
	isbnElec := row[2]
	if isbnElec != "" && isbnElec != "online_identifier" {
		isbnNoCaret := strings.ReplaceAll(isbnElec, "-", "")
		_, err := c.insertKbart.Exec(rowPos, isbnNoCaret, row[9], row[0])
		if err != nil {
			return err
		}
	}
	return nil
}

func (c context) intersect() {
	c.db.Exec(`PRAGMA wal_checkpoint`)
	c.db.Exec("CREATE INDEX koha_isbn ON koha(isbn)")
	c.db.Exec("CREATE INDEX kbart_isbn ON kbart(isbn)")

	writer := csv.NewWriter(os.Stdout)
	writer.Comma = ';'
	defer writer.Flush()
	rows, err := c.db.Query("SELECT koha.isbn, koha.cote, koha.biblionumber, kbart.url FROM koha JOIN kbart ON koha.isbn = kbart.isbn ORDER BY koha.biblionumber")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	var r = make([]string, 6)
	for rows.Next() {
		if err := rows.Scan(&r[0], &r[1], &r[2], &r[3]); err != nil {
			panic(err)
		}
		r[4] = strings.ReplaceAll(r[3], "www.cairn.info/", "www-cairn-info.ressources.sciencespo-lyon.fr/")
		r[5] = fmt.Sprintf("https://sigb.sciencespo-lyon.fr/cgi-bin/koha/cataloguing/addbiblio.pl?biblionumber=%s", r[2])
		writer.Write(r)
	}
}
